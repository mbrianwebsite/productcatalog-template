import "./App.css";
import React from "react";
import Catalog from "./components/Catalog";
import Form from "./components/Form";
import { ProductContextProvider } from "./context/ProductContext";

function App() {
  return (
    <div>
      <ProductContextProvider>
        <Form />
        <Catalog />
      </ProductContextProvider>
    </div>
  );
}

export default App;
