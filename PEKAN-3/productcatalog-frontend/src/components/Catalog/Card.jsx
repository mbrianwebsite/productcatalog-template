import React from "react";

function Card({ product }) {
  return (
    <div className="relative flex flex-col rounded-b-lg shadow-lg">
      <img
        className="object-cover rounded-t-lg h-60"
        src={product.image_url}
        alt=""
      />
      <div className="flex flex-col p-2 sm:p-4 ">
        <div className="font-sans font-bold text-base  sm:text-lg text-[#394F87]">
          {product.name}
        </div>
        <div className="font-sans font-bold text-md  sm:text-xl text-red-500 line-through h-[24px]">
          {product.is_diskon ? product.harga_display : null}
        </div>
        <div className="font-sans font-bold text-md  sm:text-xl text-green-900">
          {product.is_diskon
            ? product.harga_diskon_display
            : product.harga_display}
        </div>
        <div className="font-sans font-normal text-xs  sm:text-base text-[#696969]">
          Stok : {product.stock}
        </div>
      </div>
      <a
        href="/detail.html"
        className="hidden sm:block absolute bottom-2 right-2  sm:bottom-4 sm:right-4 px-2 py-1 bg-[#394F87] rounded-md sm:rounded-lg"
      >
        <div className="font-sans font-semibold text-xs sm:text-base text-white">
          Detail
        </div>
      </a>
      <a href="#" className="sm:hidden px-2 py-1 m-2 bg-[#394F87] rounded-md">
        <div className="font-sans font-semibold text-base text-white text-center">
          Detail
        </div>
      </a>
    </div>
  );
}

export default Card;
