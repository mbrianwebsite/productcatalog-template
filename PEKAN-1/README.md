# Tugas Pekan 1

Membuat sebuah tampilan website menggunakan HTML, dan TailwindCSS. Membuat tampilan berjumlah 4 halaman.

- Halaman Home (home.html)
- Halaman Product (product.html)
- Halaman Detail (detail.html)
- Halaman Form (form.html)

## 🚀 About Me

[@mbrianwebsite](https://www.github.com/mbrianwebsite) | Full-stack Website Developer
