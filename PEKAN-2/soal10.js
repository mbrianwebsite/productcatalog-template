function kuadrat(nilai) {
  return nilai * nilai;
}

function bodyMassIndex(berat, tinggi) {
  return berat / kuadrat(tinggi);
}

const result = bodyMassIndex(80, 1.8);

console.log(result);
